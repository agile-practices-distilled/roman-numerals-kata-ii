using NUnit.Framework;

namespace RomanNumerals_II
{
    public class RomanNumeralsParserShould
    {
        [TestCase(1, "I")]
        [TestCase(2, "II")]
        [TestCase(3, "III")]
        [TestCase(4, "IV")]
        [TestCase(5, "V")]
        [TestCase(6, "VI")]
        [TestCase(7, "VII")]
        [TestCase(8, "VIII")]
        [TestCase(9, "IX")]
        [TestCase(10, "X")]
        [TestCase(11, "XI")]
        [TestCase(12, "XII")]
        [TestCase(13, "XIII")]
        public void convert_decimal_numbers_to_roman(int input, string expectedOutput)
        {
            Assert.AreEqual(expectedOutput, RomanNumerals.Parse(input));
        }
    }
}