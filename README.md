# Roman Numerals kata II

Restart the Roman numerals kata, but this time apply all great habits we have looked at so far, plus TPP. Try to evolve your code using only the TPP table. Don’t think about how to implement a test. Just keep moving down the TPP table. If you do this, the problem should
solve itself. If you find yourself jumping steps, pause and consider if there is a simpler way to move forward with a simpler transformation.

Write a function to convert Arabic numbers to Roman numerals as best as you can, following the TDD practices we have been using.43
Given a positive integer number (for example, 42), determine its Roman numeral represen- tation as a string (for example, “XLII”). You cannot write numerals like IM for 999.