﻿using System;
using System.Collections.Generic;

namespace RomanNumerals_II
{
    public class RomanNumerals
    {
        private static readonly Dictionary<int,string> DecimalToRoman = new Dictionary<int, string>
        {
            { 1, "I"},
            { 4, "IV"},
            { 5, "V"},
            { 9, "IX"},
            { 10, "X"},
        };
        
        public static string Parse(int number)
        {
            if (DecimalToRoman.ContainsKey(number))
                return DecimalToRoman[number];
            
            if (number > 10)
                return Parse(10) + Parse(number - 10);
            if (number > 5)
                return Parse(5) + Parse(number - 5);
            if (number > 1)
                return Parse(1) + Parse(number - 1);

            return "";
        }
    }
}